RM = rm -rf
PATH += :$(ANDROID_NDK):.
NDK_PROJECT_PATH=$(shell pwd)/app/src/main

all:
	ndk-build -C .
	gradlew uninstallAll
	gradlew installDebug &

clean:
	gradlew uninstallAll &
	$(RM) app/build app/src/main/{libs,obj}
